# OfficeVR
Office VR: A VR project management game made using Unity3D

### System requirements: 

Literally any pc that runs VR! (Windows PC with GTX 1060 preferred) 

An Oculus Rift S VR headset (surely you can find one of those) 


### How to set up: 

Download the project and import into Unity.
Set up Steam VR with the Oculus Rift S VR headset and play.

## Game Manual: 


### Introduction: 

Project management nowadays is an essential part of the work life. Offices worldwide are constantly required to deliver projects with limited time and resources. A project manager’s job is to produce project plans that manage to deliver the maximum output in the most efficient way possible within these limitations. 

The traditional ways of project management, whether its paper plans or Gantt charts on computers, can be boring to users on a daily basis and offer very limited visualization. With our new Office VR game, we are revolutionizing project management completely by using VR technology, making it more fun and immersive than ever! We hope this manual helps guide you through our game. So, no time wasted, let’s get to it. 


 

 

### How to play: 

As the project manager of your very own office, your job will be to hire and assign employees in a way that suits your budget and allows you to finish the assigned project on time. Our game consists mainly of three workstations. Once the game starts, you will be spawned in the office. Moving around is as simple as touching the direction arrows on the ground. So, let’s look at the stations individually. 

  

### Workstation: 

  

  

Once you reach the workstation, you will be able to see tasks of the current project right in front of you. Right on the table beneath the project schedule, you will see your current available employees as bubbles. Using your artificial hands in the VR environment, you can easily grab an employee. Doing so will bring up a panel of that employee’s stats, like speed and cost. There are also small cylinders available. Each cylinder represents a task in the schedule above. Assigning an employee to work on a task is as simple as grabbing that employee bubble and inserting it in the corresponding cylinder of that task. On your right there is also a panel that shows the current cost of your project compared to your available budget. Once you think you have assigned the right employees to the right tasks, you can press the button to run the project. If you did a good job, the project would complete successfully. If it stops midway, don’t worry! All you need to do is just make a few adjustments to make sure you planned your project right! 

  

 

 

### Hiring station: 

 

Once you have completed projects successfully, your available budget will increase. And guess what, you can use that budget to hire new employees! To do that, go to the hiring station in the reception, using the arrow on the ground where you will see several employees waiting. Their stats are visible on a panel in the station. To hire them, all you simply need to do is go to them and touch them. Congratulations, you now have a new employee! Once you head back to the workstation, you will see your new employee added as a bubble on your table, ready to be assigned to a task. 

  

That’s it! You can now enjoy Office VR! Ready, set, go!! 
